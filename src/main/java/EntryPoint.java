import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.lang.Math.abs;
import static java.lang.Thread.sleep;


@Path("/entry-point")
public class EntryPoint {

    static double temp = 22;
    static double qnt = 500;
    static double qnt_ch = 500;
    static double min = 25;

    @GET
    @Path("test")
    @Produces(MediaType.TEXT_PLAIN)
    public String test(
            @QueryParam("token") String tkn){
        System.out.println(tkn);

        try {
            //sendNotification("https://fcm.googleapis.com/v1/{parent=projects/*}/messages:send","yoi","sd",1);
            TimeUnit.SECONDS.sleep(2);
            sendNotification(tkn);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "DONE";
    }


    private void sendNotification(String tkn) throws IOException {
        URL url = new URL("https://fcm.googleapis.com/fcm/send");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.setUseCaches(false);
        conn.setDoInput(true);
        conn.setDoOutput(true);

        conn.setRequestMethod("POST");
        conn.setRequestProperty("Authorization","key=AAAAZugOcVY:APA91bGXgzXE4SgGK8u4jsBAoNM2OO6Ejht5lyOyrigLlAWnPAUzXY2FLOzJBhjN8k0RM2xzantupcWcqKW3M2IfCzrkIdGExNvzm6PCJDGphEGyjNkAnCW5EZ8C857CrZ7F9LygJSW9");
        conn.setRequestProperty("Content-Type", "application/json");

        JsonObject json = new JsonObject();

        json.addProperty("to", tkn);

        JsonObject info = new JsonObject();
        info.addProperty("title", "YEAYY! Your Kettle is READY!");   // Notification title
        info.addProperty("body", "Go Get Your Tea!"); // Notification body
        //info.addProperty("image-url","null");

        json.add("notification", info);

        System.out.println(json);

        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
        wr.write(json.toString());
        wr.flush();
        InputStream stream =  conn.getInputStream();
        System.out.println(stream.toString());
    }


    @GET
    @Path("state")
    @Produces(MediaType.TEXT_PLAIN)
    public String state(){
        System.out.print(new Date().toString() + "\t\t" + "ANDROID: " + "\t\t");
        System.out.print("ESTADO REQUESITADA!\n");
        JsonArray json = new JsonArray();
        json.add(temp);
        json.add(qnt);
        json.add(qnt_ch);
        json.add(min);
        json.add(connect);
        return json.toString();
    }

// {[temp,qnt,qnt_ch,min,connect]}


    static private HashMap<String, JsonObject> requests = new HashMap<>();

    @GET
    @Path("reserve")
    @Produces(MediaType.TEXT_PLAIN)
    public String reserve(
            @QueryParam("token") String tkn,
            @QueryParam("user") String usr,
            @QueryParam("qt") double qtd
    ){
        System.out.print(new Date().toString() + "\t\t" + "ANDROID: " + "\t\t");
        System.out.println("DEVICE: " + tkn  + " REQUESTED: " + qtd + "\n");
        JsonObject rsv = new JsonObject();
        rsv.addProperty("user" ,usr);
        rsv.addProperty("qt",qtd);
        if(qtd < qnt)
            qnt-=qtd;
        else{
            JsonArray a = new JsonArray();
            a.add("No Water Available");
            return a.toString();
        }

        requests.put(tkn,rsv);
        System.out.println(requests.keySet().toArray().toString());

        JsonArray a = new JsonArray();
        a.add("Done");
        return a.toString();
    }

    @GET
    @Path("history")
    @Produces(MediaType.TEXT_PLAIN)
    public String history(){
        JsonArray json = new JsonArray();
        json.add(requests.values().toString());
        System.out.print(new Date().toString() + "\t\t" + "ANDROID: " + "\t\t");
        System.out.print("HISTORY REQUESTED\n");
        return json.toString();
    }


    @GET
    @Path("mult")
    @Produces(MediaType.TEXT_PLAIN)
    public String mult() throws IOException {

        for(String tkn:requests.keySet()){
            System.out.println("notification to " + requests.get(tkn).get("user") );
            sendNotification(tkn);
        }

        return "done";
    }


    static String connect="n";
    @GET
    @Path("arduino")
    @Produces(MediaType.TEXT_PLAIN)
    public String arduino(
            @QueryParam("wg_") double wg,
            @QueryParam("temp_") double tmp,
            @QueryParam("on_") String on
    ) throws IOException {
        System.out.print(new Date().toString() + "\t\t" + "ARDUINO: " + "\t\t");
        System.out.print("wg: "+ wg +" tmp: " + tmp + " on?: " + on + "\n");

        /*if(wg < qnt_ch){
            qnt = wg;
            qnt_ch = wg;
        }
        else */

        if(qnt_ch == wg) qnt_ch = wg;
        else{
            double dif=wg-qnt_ch;
            qnt = qnt + dif;
            qnt_ch = wg;
        }
        temp = tmp;

        if(requests.isEmpty()){
            qnt = wg;
        }

         //Agua disponivel maior que agua presente

        if(tmp > 91 && on.equals("s")){ // está ligada e acabou de ferver;
            connect = "n";
            mult();
            //qnt =  qnt_ch; //quando desliga a chaleira, agua fica toda disponivel
            //requests.clear();
        }
        return "#" +connect;
    }

    @GET
    @Path("g")
    @Produces(MediaType.TEXT_PLAIN)
    public char g(){
        System.out.println();
        return 'q';
    }


    @GET
    @Path("turn_on")
    @Produces(MediaType.TEXT_PLAIN)
    public String turn_on(
            @QueryParam("con") boolean conn,
            @QueryParam("token") String tkn
    ){

        if(conn) {
            if(qnt_ch<100) connect = "p";
            else if(!requests.containsKey(tkn)) return "t";
            else {
                connect = "s";
                System.out.print(new Date().toString() + "\t\t" + "ANDROID: " + "\t\t");
                System.out.print(requests.get(tkn).get("user") + " changed the state of the kettle to " + conn + "\n");
            }
        }
        return connect;
    }

    @GET
    @Path("collect")
    @Produces(MediaType.TEXT_PLAIN)
    public String collect(
            @QueryParam("token") String tkn
    ){
        System.out.print(new Date().toString() + "\t\t" + "ANDROID: " + "\t\t");
        System.out.print(requests.get(tkn).get("user")  + " collected the water\n");
        if(requests.containsKey(tkn)) {
            qnt = qnt + requests.get(tkn).get("qt").getAsDouble();
            requests.remove(tkn);
            return "okay";
        }
        return "something went wrong";
    }

}